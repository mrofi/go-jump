package main

import (
    "fmt"
    "strconv"
    "strings"
)

type ParkingLot struct {
    slot int
    taken map[int]Ticket
}

type Ticket struct {
    plate string
    color string
    slot int
}

func CreateParkingLot(number int) (*ParkingLot, error) {
    if number < 1 {
        return nil, fmt.Errorf("Please provide any number starting from 1")
    }

    parkingLot := ParkingLot{number, make(map[int]Ticket)}
    return &parkingLot, nil
}

func (p ParkingLot) CreateTicket(plate string, color string) (*Ticket, error) {
    if len(p.taken) == p.slot {
        return nil, fmt.Errorf("Sorry, parking lot is full")
    }

    for i := 1; i <= p.slot; i++ {
        if _, exist := p.taken[i]; !exist {
            ticket := Ticket{plate, color, i}
            p.taken[i] = ticket
            return &ticket, nil
        }
    }

    return nil, fmt.Errorf("Can't create ticket")
}

func (p ParkingLot) Leave(slot int) (bool, error) {
    _, free := p.taken[slot]
    if !free {
        return false, fmt.Errorf("Sorry, slot number %v has been freed", slot)
    }
    delete(p.taken, slot)
    _, free = p.taken[slot]
    return !free, nil;
}

func (p ParkingLot) Status() (string) {
    status := "Slot No.  Registration No  Colour\n"
    for i:= 1; i <= p.slot; i++ {
        if ticket, exist := p.taken[i]; exist {
            strSlot := strconv.Itoa(ticket.slot)
            dotSlot := 10 - len(strSlot)
            dotPlate := 17 - len(ticket.plate)
            status += strSlot + strings.Repeat(" ", dotSlot) + ticket.plate + strings.Repeat(" ", dotPlate) + ticket.color + "\n"
        }
    }
    return status
}

func (p ParkingLot) FindSlotsByColor(color string) (map[int]Ticket, error) {
    slots := make(map[int]Ticket)
    for i := 1; i <= p.slot; i++ {
        ticket := p.taken[i]
        if ticket.color == color {
            slots[i] = ticket
        }
    }

    if len(slots) > 0 {
        return slots, nil
    } else {
        return nil, fmt.Errorf("Not found")
    }

}

func (p ParkingLot) FindSlotByPlate(plate string) (int, error) {
    for i := 1; i <= p.slot; i++ {
        ticket := p.taken[i]
        if ticket.plate == plate {
            return ticket.slot, nil
        }
    }

    return 0, fmt.Errorf("Not found")
}